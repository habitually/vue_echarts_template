 import commonMixins from './common.js';

 export default {
     mixins: [commonMixins],
     props: {
         title: {
             default: '饼图I'
         },
         data: {
             default: () => [{
                 value: 1048,
                 name: '搜索引擎'
             }, {
                 value: 735,
                 name: '直接访问'
             }, {
                 value: 580,
                 name: '邮件营销'
             }, {
                 value: 484,
                 name: '联盟广告'
             }, {
                 value: 300,
                 name: '视频广告'
             }]
         }
     },
     data() {
         let self = this;
         return {
             config: {
                 tooltip: {
                     trigger: 'item'
                 },
                 series: [{
                     name: '访问来源',
                     type: 'pie',
                     radius: ['40%', '60%'],
                     avoidLabelOverlap: false,
                     itemStyle: {
                         borderRadius: 10,
                         borderColor: '#fff',
                         borderWidth: 2,
                         normal: {
                             labelLine: {
                                 show: true, //数据标签引导线
                                 length: 10,
                                 lineStyle: {
                                     width: 1,
                                     type: 'solid'
                                 }
                             }
                         }
                     },
                     emphasis: {
                         label: {
                             show: true,
                             fontSize: '14',
                             fontWeight: 'bold'
                         }
                     },
                     labelLine: {
                         show: false
                     },
                     data: self.data
                 }]
             }
         }
     },

 }