 import commonMixins from './common.js';
 
 export default {
    mixins: [commonMixins],
    props: {
        title: {
            default: '柱状图I'
        },
        xData: {
            default: () => ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
        },
        yData: {
            default: () => [10, 52, 200, 334, 390, 330, 220]
        }
    },
    data() {
        let self = this;
        return {
            config: {
                tooltip: {
                    trigger: 'axis',
                    axisPointer: { // 坐标轴指示器，坐标轴触发有效
                        type: 'shadow' // 默认为直线，可选为：'line' | 'shadow'
                    }
                },
                grid: {
                    left: '3%',
                    right: '4%',
                    bottom: '3%',
                    containLabel: true
                },
                xAxis: [{
                    type: 'category',
                    data: self.xData,
                    axisTick: {
                        alignWithLabel: true
                    }
                }],
                yAxis: [{
                    type: 'value'
                }],
                series: [{
                    name: '直接访问',
                    type: 'bar',
                    barWidth: '60%',
                    data: self.yData
                }]
            }
        }
    }
 }