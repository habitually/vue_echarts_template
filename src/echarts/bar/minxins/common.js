export default {
    props: {
        height: {
            type: [String],
            default: '300px'
        },
        width: {
            type: [String],
            default: '300px'
        }
    },
    computed: {
        chartId() {
            return 'main_' + Math.random() * 10;
        }
    },
    methods: {
        init() {
            var chartDom = document.getElementById(this.chartId);
            let myChart = this.$echarts.init(chartDom);
            this.config && myChart.setOption(this.config);
        },
    },
    watch: {
        yData: {
            handle(value) {
                if (value) this.init();
            },
            deep: true,
        },
    },
    created() {
        this.$nextTick(() => {
            this.init();
        });
    }
}